const { MessageEmbed } = require("discord.js")

exports.run = async(client, message, args) => {
   if (!message.member.permissions.has("MANAGE_MESSAGES"))
     return message.reply(
      `Você não tem permissão para este comando !`
    );
  if(!args[0]) return message.channel.send(`Escreva um texto depois do comando.`)

  message.channel.send(new MessageEmbed()
    .setTitle(args.join(" "))
    .setColor('#8517BC')
    .setThumbnail("https://astralbot-premium.s3.sao01.cloud-object-storage.appdomain.cloud/imagem_2021-08-29_215926.png")
    .setFooter(`A mensagem foi enviada por ${message.author.tag} !!`))
}