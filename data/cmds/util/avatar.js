const Discord = require('discord.js');

exports.run = async (client, message, args) => {
  let user = message.mentions.users.first() || client.users.cache.get(args[0])
  if (!user) {
    return message.reply(`Olá, mencione uma pessoa primeiro !`)
  }
 let avatar = user.avatarURL({ dynamic: true, format: "png", size: 1024 }) || `https://cdn.discordapp.com/embed/avatars/${message.author.discriminator % 5}.png`;
  let embed = new Discord.MessageEmbed()
    .setColor(`#8517BC`)
    .setTitle(` o avatar de : ${user.username}`)
    .setImage(avatar)
    .setFooter(`Autor do comando: ${message.author.tag}`, 
    message.author.displayAvatarURL({format: "jpg"}));
  await message.channel.send(embed);
};